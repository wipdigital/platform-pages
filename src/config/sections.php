<?php namespace WorkInProgess\Pages;

return [
  'active' => true,

  'title' => true,
  'short_description' => true,
  'url' => true,
  'description' => true,
  'hover_description' => false,
  'image' => true 
];
