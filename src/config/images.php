<?php namespace WorkInProgess\Pages;

return [
  'active' => true,

  'title' => true,
  'short_description' => true,
  'url' => true,
  'image' => true, 
  'template_id' => false 
];
