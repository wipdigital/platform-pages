<?php namespace WorkInProgress\Pages;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Page extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pages';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['page_id', 'template_id', 'type_id', 'title', 'seo_title', 'permalink', 'full_permalink', 'short_description', 'description', 'active', 'order', 'primary', 'secondary', 'footer'];

  static function _generateFullPermalink($page, $value, $permalink = array())
  {
    $permalink[] = $value;

    if($page->parentPage()->first() && $page->parentPage()->first()->permalink != 'home') {
      $permalink = Page::_generateFullPermalink($page->parentPage()->first(), $page->parentPage()->first()->permalink, $permalink);
    }

    return $permalink;
  }

  static function generateFullPermalink($page, $value)
  {
    $permalink = array();


    if($value == 'home') {
      return 'home';
    }

    $permalink = Page::_generateFullPermalink($page, $value, $permalink);
    $permalink = array_reverse($permalink);

    return implode('/', $permalink);
  }

  public static function boot()
  {
    parent::boot();

    Page::creating(function($page) {
      $page->full_permalink = Page::generateFullPermalink($page, $page->permalink); 
      $page->order = Page::where('page_id', '=', $page->page_id)->count()+1;
    });

    Page::updating(function($page) {
      $page->full_permalink = Page::generateFullPermalink($page, $page->permalink); 

      if(count($page->pages)) {
        foreach($page->pages as $p) {
          $p->full_permalink = Page::generateFullPermalink($p, $p->permalink); 
          $p->save();
        }
      }
    });

    Page::deleted(function($page) {
      $pages = Page::where('page_id', '=', $page->page_id)->orderBy('order', 'asc')->get();

      $i = 1;
      foreach($pages as $page) {
        $page->order = $i;
        $i++;
        $page->save();
      }
    });
  }

  public function pages()
  {
    return $this->hasMany('\WorkInProgress\Pages\Page');
  }

  public function template()
  {
    return $this->hasOne('\WorkInProgress\Pages\Template');
  }

  public function images()
  {
    return $this->hasMany('\WorkInProgress\Pages\PageImage');
  }

  public function sections()
  {
    return $this->hasMany('\WorkInProgress\Pages\PageSection');
  }

  public function type()
  {
    return $this->hasOne('\WorkInProgress\Pages\Type');
  }

  public function parentPage()
  {
    return $this->belongsTo('\WorkInProgress\Pages\Page', 'page_id');
  }

  public function getTopParentPageAttribute()
  {
    $page = Page::find($this->attributes['id']);
    while($page->parentPage()->first()->permalink != 'home') {
      $page = $page->parentPage()->first();
    }

    return $page;
  }

  public function scopePrimary($query)
  {
    return $query->where('active', '=', true)->where('primary', '=', true);
  }

  public function scopeSecondary($query)
  {
    return $query->where('active', '=', true)->where('secondary', '=', true);
  }

  public function scopeFooter($query)
  {
    return $query->where('active', '=', true)->where('footer', '=', true);
  }

  public function scopeActive($query)
  {
    return $query->where('active', '=', true);
  }
}

?>
