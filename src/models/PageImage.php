<?php namespace WorkInProgress\Pages;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PageImage extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'page_images';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['page_id', 'template_id', 'src', 'url', 'title', 'short_description', 'featured', 'active', 'order'];

  public static function boot()
  {
    parent::boot();

    PageImage::creating(function($image) {
      $image->order = PageImage::where('page_id', '=', $image->page_id)->count()+1;
    });

    PageImage::deleted(function($image) {
      $images = PageImage::where('page_id', '=', $image->page_id)->orderBy('order', 'asc')->get();

      $i = 1;
      foreach($images as $image) {
        $image->order = $i;
        $i++;
        $image->save();
      }
    });
  }

  public function page()
  {
    return $this->belongsTo('\WorkInProgress\Pages\Page');
  }

  public function template()
  {
    return $this->hasOne('\WorkInProgress\Pages\Template', 'template_id');
  }

}

?>
