<?php namespace WorkInProgress\Pages;

class Type extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'page_types';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = array('id');

}

?>
