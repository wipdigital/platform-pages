<?php namespace WorkInProgress\Pages;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PageSection extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'page_sections';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['page_id', 'src', 'url', 'description', 'featured', 'active', 'order', 'title', 'short_description', 'template_id', 'hover_description'];

  public static function boot()
  {
    parent::boot();

    PageSection::creating(function($section) {
      $section->order = PageSection::where('page_id', '=', $section->page_id)->count()+1;
    });

    PageSection::deleted(function($section) {
      $sections = PageSection::where('page_id', '=', $section->page_id)->orderBy('order', 'asc')->get();

      $i = 1;
      foreach($sections as $section) {
        $section->order = $i;
        $i++;
        $section->save();
      }
    });
  }

  public function page()
  {
    return $this->belongsTo('\WorkInProgress\Pages\Page');
  }

}

?>
