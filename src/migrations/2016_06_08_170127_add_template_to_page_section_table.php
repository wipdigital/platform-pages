<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplatetoPageSectionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('page_sections', function(Blueprint $table)
		{
      $table->integer('template_id')->unsigned()->default(4);
      $table->foreign('template_id')->references('id')->on('templates');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('page_sections', function(Blueprint $table)
		{
      $table->dropColumn('template_id');
		});
	}

}
