<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('page_types', function(Blueprint $table)
		{
			$table->increments('id');
      $table->string('title');
      $table->string('platform_permalink');
      $table->string('client_permalink');
      $table->boolean('edit')->default(true);
      $table->boolean('delete')->default(true);
			$table->timestamps();
      $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('page_types');
	}

}
