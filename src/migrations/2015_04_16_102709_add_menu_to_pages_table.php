<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMenuToPagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pages', function(Blueprint $table)
		{
      $table->boolean('primary')->default(true)->after('order');
      $table->boolean('secondary')->default(true)->after('primary');
      $table->boolean('footer')->default(false)->after('secondary');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pages', function(Blueprint $table)
		{
      $table->drop_column('primary');
      $table->drop_column('secondary');
      $table->drop_column('footer');
		});
	}

}
