<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageSnippetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('page_snippets', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('page_id')->unsigned()->nullable()->default(1);
      $table->foreign('page_id')->references('id')->on('pages');
      $table->string('title');
      $table->string('description');
      $table->boolean('active')->default(true);
			$table->timestamps();
      $table->softDeletes();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::drop('page_snippets');
	}

}
