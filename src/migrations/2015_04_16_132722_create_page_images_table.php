<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('page_images', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('page_id')->unsigned()->nullable()->default(1);
      $table->foreign('page_id')->references('id')->on('pages');
      $table->integer('template_id')->unsigned();
      $table->foreign('template_id')->references('id')->on('templates');
      $table->text('src');
      $table->text('url');
      $table->text('title');
      $table->text('short_description');
      $table->boolean('featured')->default(false);
      $table->boolean('active')->default(true);
      $table->integer('order');
			$table->timestamps();
      $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('page_images');
	}

}
