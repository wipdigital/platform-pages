<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleColumnToPageSectionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('page_sections', function(Blueprint $table)
		{
      $table->text('short_description');
      $table->text('title');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('page_sections', function(Blueprint $table)
		{
      $table->dropColumn('short_description');
      $table->dropColumn('title');
		});
	}

}
