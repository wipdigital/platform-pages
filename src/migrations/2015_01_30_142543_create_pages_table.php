<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('page_id')->unsigned()->nullable()->default(1);
      $table->foreign('page_id')->references('id')->on('pages');
      $table->integer('template_id')->unsigned()->default(1);
      $table->foreign('template_id')->references('id')->on('templates');
      $table->integer('type_id')->unsigned()->nullable();
      $table->foreign('type_id')->references('id')->on('page_types');
      $table->string('title');
      $table->string('seo_title');
      $table->string('permalink');
      $table->string('full_permalink');
      $table->text('short_description');
      $table->text('description');
      $table->boolean('active')->default(true);
      $table->integer('order');
			$table->timestamps();
      $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}

?>
