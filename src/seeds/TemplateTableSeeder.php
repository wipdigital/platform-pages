<?php namespace WorkInProgress\Pages;

class TemplateTableSeeder extends \Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    \DB::table('templates')->delete();

    Template::create(array(
      'name' => 'Internal',
      'src' => 'internal',
      'type' => 1
    ));

    Template::create(array(
      'name' => 'Home',
      'src' => 'home',
      'type' => 1
    ));

    Template::create(array(
      'name' => 'Slider',
      'src' => 'slider',
      'type' => 2
    ));

    Template::create(array(
      'name' => 'Contact',
      'src' => 'contact',
      'type' => 1
    ));
	}

}
