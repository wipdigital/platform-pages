<?php namespace WorkInProgress\Pages;

class PageTableSeeder extends \Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    \DB::table('pages')->delete();

    Page::create(array(
      'page_id' => null,
      'title' => 'Home',
      'permalink' => 'home',
      'full_permalink' => 'home',
      'template_id' => 2,
      'order' => 1,
      'description' => '<h1>Welcome to Work In Progress</h1>'
    ));

	}

}
