<?php namespace WorkInProgress\Pages;

class PageImageController extends \BaseController {

	private $rules = array(
    'page_id' => 'required'
  );

  public function __construct()
  {
    $this->beforeFilter('auth');
  }

  public function index($full_permalink)
  {
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();
    $template_ids = Template::where('type', 4)->lists('name', 'id');

    $data = [
      'page' => $page,
      'template_ids' => $template_ids
    ];

    return \View::make('pages::page.images', $data);
  }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($full_permalink)
	{
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();
    $template_ids = Template::where('type', 4)->lists('name', 'id');

    $data = \Input::all();
    $data['page_id'] = $page->id;
    $data['featured'] = (\Input::has('featured')) ? true : false;
    $data['active'] = (\Input::has('active')) ? true : false;

    $validator = \Validator::make($data, $this->rules);

    if($validator->fails()) {
      return \Redirect::back()->withErrors($validator)->withInput();
    }

    if($image = PageImage::create($data)) {

      $data = [
        'page' => $page,
        'image' => $image,
        'template_ids' => $template_ids
      ];

      return \View::make('pages::partials.image', $data);
    }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($full_permalink, $id)
	{
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();
    $template_ids = Template::where('type', 4)->lists('name', 'id');

    $image= PageImage::findOrFail($id);

    $data = \Input::all();
    $data['page_id'] = $page->id;
    $data['featured'] = (\Input::has('featured')) ? true : false;
    $data['active'] = (\Input::has('active')) ? true : false;

    $validator = \Validator::make($data, $this->rules);

    if($validator->fails()) {
      return \Redirect::back()->withErrors($validator)->withInput();
    }

    if($image->update($data)) {

      $data = [
        'page' => $page,
        'image' => $image,
        'template_ids' => $template_ids
      ];

      return \View::make('pages::partials.image', $data);
    }
	}


  public function delete($full_permalink, $id) {
    $image = PageImage::findOrFail($id);

    if($image->delete()) {
      $images = PageImage::where('page_id', '=', $image->page_id)->orderBy('order', 'asc')->orderBy('updated_at', 'desc')->get();
      \Session::flash('message', 'Page Image successfully deleted!');
      return \Redirect::back();
    }
  }

  public function order($id)
  {
    $image = PageImage::findOrFail($id);
    $order = $image->order;
    $image->order = \Input::get('order');
    $image->save();

    if($order > $image->order) {
      $images = PageImage::where('page_id', '=', $image->page_id)->orderBy('order', 'asc')->orderBy('updated_at', 'desc')->get();
    } else {
      $images = PageImage::where('page_id', '=', $image->page_id)->orderBy('order', 'asc')->orderBy('updated_at', 'asc')->get();
    }

    $i = 1;
    foreach($images as $image) {
      $image->order = $i;
      $i++;
      $image->save();
    }

    \Session::flash('message', 'Page Image order successfully updated!');
    return \Redirect::back();
  }

}
