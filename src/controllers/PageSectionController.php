<?php namespace WorkInProgress\Pages;

class PageSectionController extends \BaseController {

	private $rules = [ 
    'page_id' => 'required',
    'template_id' => 'required'
  ];

  public function __construct()
  {
    $this->beforeFilter('auth');
  }

  public function index($full_permalink)
  {
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $template_options = Template::where('type', '=', 4)->get()->lists('name', 'id');

    $data = [
      'page' => $page,
      'template_options' => $template_options
    ];

    return \View::make('pages::page.sections', $data);
  }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($full_permalink)
	{
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $data = \Input::all();
    $data['page_id'] = $page->id;
    $data['featured'] = (\Input::has('featured')) ? true : false;
    $data['active'] = (\Input::has('active')) ? true : false;

    $validator = \Validator::make($data, $this->rules);

    if($validator->fails()) {
      return \Redirect::back()->withErrors($validator)->withInput();
    }

    if($section = PageSection::create($data)) {
      $template_options = Template::where('type', '=', 4)->get()->lists('name', 'id');

      $data = [
        'page' => $page,
        'section' => $section,
        'template_options' => $template_options

      ];

      return \View::make('pages::partials.section', $data);
    }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($full_permalink, $id)
	{
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $section = PageSection::findOrFail($id);

    $data = \Input::all();
    $data['page_id'] = $page->id;
    $data['featured'] = (\Input::has('featured')) ? true : false;
    $data['active'] = (\Input::has('active')) ? true : false;

    $validator = \Validator::make($data, $this->rules);

    if($validator->fails()) {
      return \Redirect::back()->withErrors($validator)->withInput();
    }

    if($section->update($data)) {
      $template_options = Template::where('type', '=', 4)->get()->lists('name', 'id');

      $data = [
        'page' => $page,
        'section' => $section,
        'template_options' => $template_options
      ];

      return \View::make('pages::partials.section', $data);
    }
	}


  public function delete($full_permalink, $id) {
    $section = PageSection::findOrFail($id);

    if($section->delete()) {
      $sections = PageSection::where('page_id', '=', $section->page_id)->orderBy('order', 'asc')->orderBy('updated_at', 'desc')->get();
      \Session::flash('message', 'Page Section successfully deleted!');
      return \Redirect::back();
    }
  }

  public function order($id)
  {
    $section = PageSection::findOrFail($id);
    $order = $section->order;
    $section->order = \Input::get('order');
    $section->save();

    if($order > $section->order) {
      $sections = PageSection::where('page_id', '=', $section->page_id)->orderBy('order', 'asc')->orderBy('updated_at', 'desc')->get();
    } else {
      $sections = PageSection::where('page_id', '=', $section->page_id)->orderBy('order', 'asc')->orderBy('updated_at', 'asc')->get();
    }

    $i = 1;
    foreach($sections as $section) {
      $section->order = $i;
      $i++;
      $section->save();
    }

    \Session::flash('message', 'Page Section order successfully updated!');
    return \Redirect::back();
  }

}
