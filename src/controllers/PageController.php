<?php namespace WorkInProgress\Pages;

class PageController extends \BaseController {

	private $rules = [ 
    'title' => 'required'
  ];

  /*private function checkFullPermalink($permalink)
  {
    $page = Page::where('permalink', '=', $permalink)->first();

    if($page->id) {
      return false;
    }

    return true;
  }*/

  private function getHierarchy($page_id = 0, $depth = 0, $template = 'pages::partials.tr', $selected = 1)
  {
    $hierarchy = '';

    if($page_id) {
      $pages = Page::where('id', '=', $page_id)->orderBy('order', 'asc')->get();
    } else {
      $pages = Page::whereNull('page_id')->orderBy('order', 'asc')->get();
    }

    foreach($pages as $page) {

      $data = array(
        'depth' => $depth,
        'page' => $page,
        'selected' => $selected,
        'max_order' => Page::where('page_id', '=', $page->page_id)->count()
      );

      $hierarchy .= \View::make($template, $data);
      foreach($page->pages()->orderBy('order', 'asc')->get() as $page) {
        $hierarchy .= $this->getHierarchy($page->id, $depth+1, $template, $selected);
      }
    }

    //@TODO This should be cached in redis at some point
    return $hierarchy;
  }

  public function __construct()
  {
    $this->beforeFilter('auth');
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    $data = [ 
      'hierarchy' => $this->getHierarchy()
    ];

    return \View::make('pages::index', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
  {
    $template_options = Template::where('type', '=', 1)->get()->lists('name', 'id');

    $data = [
      'options' => $this->getHierarchy(0, 0, 'pages::partials.option', \Input::old('page_id') ?: null),
      'template_options' => $template_options
    ];

    return \View::make('pages::page.create', $data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    $data = \Input::all();
    $data['active'] = (\Input::has('active')) ? true : false;
    $data['primary'] = (\Input::has('primary')) ? true : false;
    $data['secondary'] = (\Input::has('secondary')) ? true : false;
    $data['footer'] = (\Input::has('footer')) ? true : false;

    $rules = $this->rules;
    $rules['permalink'] = 'required|unique:pages,permalink,NULL,id,page_id,' . $data['page_id'] . ',deleted_at,NULL';
    $validator = \Validator::make($data, $rules);

    if($validator->fails()) {
      return \Redirect::route('page.create')->withErrors($validator)->withInput();
    }

    if($page = Page::create($data)) {
      if(isset($data['redirect_images'])) {
        return \Redirect::to('/page/' . $page->full_permalink . '/images');
      }

      \Session::flash('message', 'Page successfully created!');
      return \Redirect::to('pages');
    }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return \Redirect::to('pages');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string $permalink
	 * @return Response
	 */
	public function edit($full_permalink)
  {
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $template_options = Template::where('type', '=', '1')->get()->lists('name', 'id');

    $data = array(
      'page' => $page,
      'options' => $this->getHierarchy(0, 0, 'pages::partials.option', \Input::old('page_id') ?: $page->page_id),
      'template_options' => $template_options
    );

    return \View::make('pages::page.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($full_permalink)
	{
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $data = \Input::all();
    $data['active'] = (\Input::has('active')) ? true : false;
    $data['primary'] = (\Input::has('primary')) ? true : false;
    $data['secondary'] = (\Input::has('secondary')) ? true : false;
    $data['footer'] = (\Input::has('footer')) ? true : false;

    $rules = $this->rules;

    if($page->permalink != 'home') {
      $rules['page_id'] = 'required|not_in:' . $page->id;
      $rules['permalink'] = 'required|unique:pages,permalink,' . $page->id . ',id,page_id,' . $data['page_id'] . ',deleted_at,NULL';
    }

    $validator = \Validator::make($data, $rules);

    if($validator->fails()) {
      return \Redirect::route('page.edit', $full_permalink)->withErrors($validator)->withInput();
    }

    $page_id = $page->page_id;
    if($page->update($data)) {
      //if the page has moved
      if(isset($data['page_id']) && $page_id != $data['page_id']) {
        $page->order = Page::where('page_id', '=', $page->page_id)->count();
        $page->save();

        $pages = Page::where('page_id', '=', $page_id)->orderBy('order', 'asc')->get();
        $i = 1;
        foreach($pages as $page) {
          $page->order = $i;
          $i++;
          $page->save();
        }
      }

      \Session::flash('message', 'Page successfully updated!');
      return \Redirect::to('pages');
    }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function delete($full_permalink) {
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();

    if($page->delete()) {
      \Session::flash('message', 'Page successfully deleted!');
      return \Redirect::to('pages');
    }
  }

  public function order($full_permalink)
  {
    $page = Page::where('full_permalink', '=', $full_permalink)->firstOrFail();
    $order = $page->order;
    $page->order = \Input::get('order');
    $page->save();

    if($order > $page->order) {
      $pages = Page::where('page_id', '=', $page->page_id)->orderBy('order', 'asc')->orderBy('updated_at', 'desc')->get();
    } else {
      $pages = Page::where('page_id', '=', $page->page_id)->orderBy('order', 'asc')->orderBy('updated_at', 'asc')->get();
    }

    $i = 1;
    foreach($pages as $page) {
      $page->order = $i;
      $i++;
      $page->save();
    }

    \Session::flash('message', 'Page order successfully updated!');
    return \Redirect::to('pages');
  }

}
