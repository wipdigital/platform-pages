<?php

return [
  'permalink' => 'This is the URL the page will display on.',
  'page_id' => 'Page in which this page will appear under in navigation menus.',
  'template_id' => 'The layout this page will be based on.',
  'short_description' => 'Brief overview of the page diplayed in the site map or in search results.',
  'attributes' => 'Active pages are accessible by the public.',
  'navigation' => 'Which menus can this page be located in?'
];
