@if($errors->first())
  <div class="alert-box">
    <h2>Error</h2>

    @foreach($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
  </div>
@endif

<div class="title-field @if($errors->has('title'))error @endif">
  {{ Form::label('title', 'Title') }}
  {{ Form::text('title', null, ['class' => 'radius', 'required']) }}
  @if($errors->has('title'))
  <small class="error">{{ $errors->first('title') }}</small>
  @else
  <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Title']) }}</small>
  @endif
</div>

@if(!(isset($page) && $page->permalink == 'home'))
  <div class="page-id-field @if($errors->has('page_id'))error @endif">
    <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('pages::page.page_id') }}"><i class="fa fa-question-circle"></i></span>
    {{ Form::label('page_id', 'Parent Page') }}
    <select name="page_id" class="radius" required>
      {{ $options }}
    </select>
    @if($errors->has('page_id'))
    <small class="error">{{ $errors->first('page_id') }}</small>
    @else
    <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Parent Page']) }}</small>
    @endif
  </div>
@endif

<div class="short-description-field @if($errors->has('short_description'))error @endif">
  <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('pages::page.short_description') }}"><i class="fa fa-question-circle"></i></span>
  {{ Form::label('short_description', 'Short Description') }}
  {{ Form::text('short_description', null, ['class' => 'radius']) }}
  @if($errors->has('short_description'))
  <small class="error">{{ $errors->first('short_description') }}</small>
  @else
  <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Short Description']) }}</small>
  @endif
</div>

<div class="content-editor"> 
  {{ Form::label('description', 'Content') }}
  {{ Form::textarea('description', null, array('class' => 'froala-textarea')) }}
</div>

<dl class="accordion" data-accordion>
  <dd class="accordion-navigation @if($errors->has('seo_title') or $errors->has('permalink')) active @endif">
    <a href="#panel1b">Advanced Settings <i class="fa fa-caret-down"></i></a>
    <div id="panel1b" class="content @if($errors->has('seo_title') or $errors->has('permalink')) active @endif">
      <div class="seo-title-field @if($errors->has('seo_title'))error @endif">
        {{ Form::label('seo_title', 'SEO Title') }}
        {{ Form::text('seo_title', null, ['class' => 'radius']) }}
        @if($errors->has('seo_title'))
        <small class="error">{{ $errors->first('seo_title') }}</small>
        @else
        <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'SEO Title']) }}</small>
        @endif
      </div>

      @if(!(isset($page) && $page->permalink == 'home'))
        <div class="permalink-field @if($errors->has('permalink'))error @endif">
          <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('pages::page.permalink') }}"><i class="fa fa-question-circle"></i></span>
          {{ Form::label('permalink', 'Permalink') }}
          {{ Form::text('permalink', null, ['class' => 'radius', 'required']) }}
          @if($errors->has('permalink'))
          <small class="error">{{ $errors->first('permalink') }}</small>
          @else
          <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Permalink']) }}</small>
          @endif
        </div>
        <div class="template-id-field @if($errors->has('template_id'))error @endif">
          <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('pages::page.template_id') }}"><i class="fa fa-question-circle"></i></span>
          {{ Form::label('template_id', 'Template') }}
          {{ Form::select('template_id', $template_options, null, ['required']) }}
          @if($errors->has('template_id'))
          <small class="error">{{ $errors->first('template_id') }}</small>
          @else
          <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Template']) }}</small>
          @endif
        </div>
      @endif
    </div>
  </dd>
</dl>

<div class="row max-width collapse switch-label">
  <div class="small-12 medium-6 columns">
    <h3 class="states">ATTRIBUTES
      <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('pages::page.attributes') }}"><i class="fa fa-question-circle"></i></span>
    </h3>
    {{ Form::label('active', '', ['class' => 'inline left']) }}
    <div class="switch tiny left">
      {{ Form::checkbox('active', true, false) }}
      <label for="active"></label>
    </div> 
  </div>
  <div class="small-12 medium-6 columns">
    <h3 class="states">NAVIGATION MENUS
      <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('pages::page.navigation') }}"><i class="fa fa-question-circle"></i></span>
    </h3>
    <div class="row">
      <div class="small-12 columns">
        <div class="primary-field">
          {{ Form::label('primary', '', ['class' => 'inline left']) }}
          <div class="switch tiny left">
            {{ Form::checkbox('primary', true, true) }}
            <label for="primary"></label>
          </div> 
        </div>
      </div>
      <div class="small-12 columns">
        <div class="secondary-field">
          {{ Form::label('secondary', '', ['class' => 'inline left']) }}
          <div class="switch tiny left">
            {{ Form::checkbox('secondary', true, true) }}
            <label for="secondary"></label>
          </div> 
        </div>
      </div>
      <div class="small-12 columns">
        <div class="footer-field">
          {{ Form::label('footer', '', ['class' => 'inline left']) }}
          <div class="switch tiny left">
            {{ Form::checkbox('footer', true, false) }}
            <label for="footer"></label>
          </div>
        </div> 
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="froala-responsive-filemanager-url">
<div id="froala-responsive-filemanager" class="reveal-modal" data-reveal aria-labelledby="File Manager" aria-hidden="true" role="dialog">
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
  <iframe src="/responsive_file_manager/filemanager/dialog.php?type=1&field_id=froala-responsive-filemanager-url"></iframe>
</div>

@section('inline_js')
<script>
(function(jQuery) {

 var convertToSlug = function(str) {
    return str
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
  } 

  $('#title').on('input', function() {
    $('#permalink').val(convertToSlug($(this).val()));
  });

})(jQuery);
</script>
@stop
