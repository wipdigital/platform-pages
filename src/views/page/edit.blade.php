@extends('layouts.standard')

@section('main')
{{ Form::model($page, ['route' => ['page.update', $page->full_permalink], 'method' => 'PUT', 'data-abide']) }}
  <aside class="right fxd-mobile">
    @if(Config::get('pages::images.active'))
    <a href="/page/{{ $page->full_permalink }}/images" title="Images" class="button tiny radius"><i class="fa fa-files-o"></i> Images</a>
    @endif
    @if(Config::get('pages::sections.active'))
    <a href="/page/{{ $page->full_permalink }}/sections" title="Sections" class="button tiny radius"><i class="fa fa-files-o"></i> Sections</a>
    @endif
  </aside>

  <h1>Edit Page</h1>

  @include('pages::page.form')

  <div class="row max-width collapse">
    <div class="small-12 save-col columns">
      <div class="right">
        {{ Form::submit('Save Page', ['class' => 'radius right button']) }}
      </div>
    </div>
  </div>
{{ Form::close() }}
@stop
