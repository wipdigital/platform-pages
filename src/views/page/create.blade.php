@extends('layouts.standard')

@section('main')
{{ Form::open(['url' => 'page', 'data-abide']) }}
  <h1>Create Page</h1>

  @include('pages::page.form')

  <div class="row max-width collapse">
    <div class="small-12 save-col columns">
      <div class="right">
        {{ Form::submit('Create Page', ['class' => 'radius right button']) }}
        {{ Form::submit('Add Images', ['class' => 'radius button add-images', 'name' => 'redirect_images']) }}
      </div>
    </div>
  </div>
{{ Form::close() }}

@stop
