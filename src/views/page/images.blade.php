@extends('layouts.standard')

@section('main')
  <aside class="right fxd-mobile">
    <a href="/page/{{$page->full_permalink }}/edit" title="Edit" class="button tiny radius"><i class="fa fa-pencil"></i> Page</a>
  </aside>

  <h2>Images</h2>

  @foreach($page->images as $image)
    @include('pages::partials.image')
  @endforeach

  <div class="row separator max-width image-add-row">
    <div class="cent-align valign-cent loading"><i class="fa fa-cog fa-spin"></i></div>
    {{ Form::open(['url' => '/page/' . $page->full_permalink . '/image', 'class' => 'image image-add', 'data-abide']) }}
      {{ Form::hidden('template_id', 3) }}

      <div class="small-12 columns">
        <h3>New Image</h3>
      </div>

      <div class="small-12 columns">
        <div class="row">
          @if(Config::get('pages::images.image'))
          <div class="small-12 medium-6 large-4 columns">
            <div class="image-well panel radius">
              <a href="#" class="drop-area radius square" data-reveal-id="image-add-modal">
                <div class="interaction valign-cent cent-align">
                  {{ Form::hidden('src', null, ['id' => 'image-add']) }}
                  <button class="button tiny">Browse</button>
                </div>
              </a>
            </div>
          </div>
          @endif

          <div class="small-12 medium-6 large-8 columns">
            @if(Config::get('pages::images.title'))
            <div class="title-field @if($errors->has('title'))error @endif">
              {{ Form::label('title', 'Title') }}
              {{ Form::text('title', null, ['class' => 'radius']) }}
              <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Title']) }}</small>
            </div>
            @endif

            @if(Config::get('pages::images.short_description'))
            <div class="short-description-field @if($errors->has('short_description'))error @endif">
              {{ Form::label('short_description', 'Short Description') }}
              {{ Form::text('short_description', null, ['class' => 'radius']) }}
              <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Short Description']) }}</small>
            </div>
            @endif

            @if(Config::get('pages::images.url'))
            <div class="url-field @if($errors->has('url'))error @endif">
              {{ Form::label('url', 'URL') }}
              {{ Form::text('url', null, ['class' => 'radius']) }}
              @if($errors->has('url'))
              <small class="error">{{ $errors->first('url') }}</small>
              @endif
              <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'URL']) }}</small>
            </div>
            @endif

            @if(Config::get('pages::images.template_id'))
            <div class="template-id-field @if($errors->has('url'))error @endif">
              {{ Form::label('template_id', 'Template') }}
              {{ Form::select('template_id', $template_ids, null, ['class' => 'radius']) }}
              @if($errors->has('template_id'))
              <small class="error">{{ $errors->first('template_id') }}</small>
              @endif
              <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Template']) }}</small>
            </div>
            @endif

            {{ Form::submit('Create Image', ['class' => 'button tiny success']) }}

            <div id="image-add-modal" class="reveal-modal" data-reveal aria-labelledby="File Manager" aria-hidden="true" role="dialog">
              <a class="close-reveal-modal" aria-label="Close">&#215;</a>
              <iframe src="/responsive_file_manager/filemanager/dialog.php?type=1&field_id=image-add"></iframe>
            </div>
          </div>
        </div>
      </div>
    {{ Form::close() }}
  </div>
@stop

@section('inline_js')
<script>
$(document).on('submit', 'form.image:not(.image-add)', function(e) {
  $el = $(this);
  e.preventDefault();

  $el.find('button[type=submit], input[type=submit]').prop('disabled', true);
  $el.parents('.separator').addClass('loading');
  $.post($el.attr('action'), $el.serialize(), function(res) {
    $el.parents('.separator').removeClass('loading');
    $el.find('button[type=submit], input[type=submit]').prop('disabled', false);
  });
});

$(document).on('submit', 'form.image-add', function(e) {
  $el = $(this);
  e.preventDefault();

  $el.find('button[type=submit], input[type=submit]').prop('disabled', true);
  $el.parents('.separator').addClass('loading');
  $.post($el.attr('action'), $el.serialize(), function(res) {
    $el.parents('.separator').removeClass('loading');
    $(res).insertBefore($el.parents('.image-add-row'));
    $el.find('input[type=text], .image-well input[type=hidden]').val('')
    $el.find('.drop-area').css('backgroundImage', '');
    $el.find('button[type=submit], input[type=submit]').prop('disabled', false);
  });
});

$(document).on('click', 'form.image .delete', function(e) {
  $el = $(this);
  e.preventDefault();

  $.post($el.attr('href'), {'_method': 'DELETE'});

  $el.parents('.separator').slideUp(function() {
    $el.parents('.separator').remove();
  });
  $el.prop('disabled', true);
  $el.parents('.separator').addClass('loading');
});
</script>
@stop
