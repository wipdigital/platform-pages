@extends('layouts.standard')

@section('main')
  <aside class="right">
    <a href="/page/create" title="Create" class="radius tiny button"><i class="fa fa-file-text"></i> Create Page</a>
  </aside>

  <h1>Pages</h1>

  @if(Session::has('message'))
  <div data-alert class="alert-box success">
    {{ Session::get('message') }}
    <a href="#" class="close">&times;</a>
  </div>
  @endif

  <div class="hierarchy">
    @if($hierarchy)
      {{ $hierarchy }}
    @else
    <div class="small-12 columns">
      <em>No pages to display.</em>
    </div>
    @endif
  </div>

  @include('components.confirmation')
@stop
