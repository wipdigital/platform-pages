<div class="medium-3 columns image-add-{{ $image->id }} image-card">
  {{ Form::model($image, ['route' => ['pages.image.update', $image->id]]) }}
    {{ Form::hidden('_method', 'PUT') }}

    <div class="image-well panel radius">
      <a class="btn right delete" href="/pages/image/{{ $image->id }}"><i class="fa fa-close"></i></a>
      <a href="#" class="drop-area radius square" data-reveal-id="image-add-modal-{{ $image->id }}" style="background-image: url('{{ $image->src }}');">
        <div class="interaction valign-cent cent-align">
          {{ Form::hidden('page_id') }}
          {{ Form::hidden('template_id') }}
          {{ Form::hidden('src', $image->src, ['id' => 'image-add-' . $image->id, 'class' => 'src']) }}
          <button class="button tiny">Browse</button>
        </div>
      </a>
      <div class="left-align">
      {{ Form::label('title', 'Title') }}
      {{ Form::text('title') }}

      {{ Form::label('short_description', 'Short Description') }}
      {{ Form::textarea('short_description') }}

      {{ Form::label('url', 'URL') }}
      {{ Form::text('url') }}

      {{ Form::button('Save', ['type' => 'submit', 'class' => 'radius tiny expand']) }}
      </div>
    </div>
  {{ Form::close() }}

  <div id="image-add-modal-{{ $image->id }}" class="reveal-modal" data-reveal aria-labelledby="File Manager" aria-hidden="true" role="dialog">
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    <iframe src="/responsive_file_manager/filemanager/dialog.php?type=1&field_id=image-add-{{ $image->id }}"></iframe>
  </div>
</div>
