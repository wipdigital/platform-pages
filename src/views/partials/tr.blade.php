<div class="hammer row max-width">
  <div class="small-12 medium-7 columns">
    @for ($i=0; $i<$depth; $i++)<span class="hierarchy-space"></span>@endfor <a href="/page/{{ $page->full_permalink }}/edit" title="Edit {{ $page->title }}" class="@if(!$page->active)inactive @endif">{{ $page->title }}</a> @if(!$page->active) <small>(DISABLED)</small> @endif
  </div>
  <div class="small-12 medium-5 columns swiper table-right">
    {{ Form::open(array('action' => array('WorkInProgress\Pages\PageController@order', $page->full_permalink), 'method' => 'PUT', 'class' => 'order')) }}
      {{ Form::selectRange('order', 1, $max_order, $page->order) }}
    {{ Form::close() }}

    <a href="/page/{{ $page->full_permalink }}/edit" title="Edit" class="edit button tiny secondary"><i class="fa fa-pencil"></i><span class="show-for-large-up">Edit</span></a>
    <a href="/page/{{ $page->full_permalink }}/delete" title="Delete" class="delete button tiny alert" data-entry="page"><i class="fa fa-trash"></i><span class="show-for-large-up">Delete</span></a>
  </div>
</div>
