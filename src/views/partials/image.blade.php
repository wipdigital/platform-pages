<div class="row max-width separator">
  <div class="cent-align valign-cent loading"><i class="fa fa-cog fa-spin"></i></div>
  {{ Form::model($image, ['url' => '/page/' . $page->full_permalink . '/image/' . $image->id, 'method' => 'PUT', 'class' => 'image', 'data-abide']) }}
    {{ Form::hidden('template_id', 3) }}

    <div class="small-12 columns">
      <div class="row">
        @if(Config::get('pages::images.image'))
        <div class="small-12 medium-6 large-4 columns">
          <div class="image-well panel radius">
            <a href="#" class="drop-area radius square" data-reveal-id="image-add-modal-{{ $image->id }}" style="background-image: url('{{ $image->src }}');">
              <div class="interaction valign-cent cent-align">
                {{ Form::hidden('src', $image->src, ['id' => 'image-add-' . $image->id]) }}
                <button class="button tiny">Browse</button>
              </div>
            </a>
          </div>
        </div>
        @endif

        <div class="small-12 medium-6 large-8 columns">
          @if(Config::get('pages::images.title'))
          <div class="title-field @if($errors->has('title'))error @endif">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, ['class' => 'radius']) }}
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Title']) }}</small>
          </div>
          @endif

          @if(Config::get('pages::images.short_description'))
          <div class="short-description-field @if($errors->has('short_description'))error @endif">
            {{ Form::label('short_description', 'Short Description') }}
            {{ Form::text('short_description', null, ['class' => 'radius']) }}
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Short Description']) }}</small>
          </div>
          @endif

          @if(Config::get('pages::images.url'))
          <div class="url-field @if($errors->has('url'))error @endif">
            {{ Form::label('url', 'URL') }}
            {{ Form::text('url', null, ['class' => 'radius']) }}
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'URL']) }}</small>
          </div>
          @endif

          @if(Config::get('pages::images.template_id'))
          <div class="template-id-field @if($errors->has('url'))error @endif">
            {{ Form::label('template_id', 'Template') }}
            {{ Form::select('template_id', $template_ids, null, ['class' => 'radius']) }}
            @if($errors->has('template_id'))
            <small class="error">{{ $errors->first('template_id') }}</small>
            @endif
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Template']) }}</small>
          </div>
          @endif

          {{ Form::submit('Save', ['class' => 'button tiny success']) }}
          <a class="button tiny alert delete" href="/page/{{ $page->full_permalink }}/image/{{ $image->id }}"><i class="fa fa-trash"></i> Delete</a>

          <div id="image-add-modal-{{ $image->id }}" class="reveal-modal" data-reveal aria-labelledby="File Manager" aria-hidden="true" role="dialog">
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <iframe src="/responsive_file_manager/filemanager/dialog.php?type=1&field_id=image-add-{{ $image->id }}"></iframe>
          </div>
        </div>
      </div>
    </div>
  {{ Form::close() }}
</div>
