<div class="row max-width separator">
  <div class="cent-align valign-cent loading"><i class="fa fa-cog fa-spin"></i></div>
  {{ Form::model($section, ['url' => '/page/' . $page->full_permalink . '/section/' . $section->id, 'method' => 'PUT', 'class' => 'image', 'data-abide']) }}
    {{ Form::hidden('template_id', 3) }}

    <div class="small-12 columns">
      <div class="row">
        @if(Config::get('pages::sections.image'))
        <div class="small-12 medium-6 large-4 columns">
          <div class="image-well panel radius">
            <a href="#" class="drop-area radius square" data-reveal-id="image-add-modal-{{ $section->id }}" style="background-image: url('{{ $section->src }}');">
              <div class="interaction valign-cent cent-align">
                {{ Form::hidden('src', $section->src, ['id' => 'image-add-' . $section->id]) }}
                <button class="button tiny">Browse</button>
              </div>
            </a>
          </div>
        </div>
        @endif

        <div class="small-12 medium-6 large-8 columns">
          @if((bool)Config::get('pages::sections.title'))
          <div class="title-field @if($errors->has('title'))error @endif">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, ['class' => 'radius']) }}
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Title']) }}</small>
          </div>
          @endif

          <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('pages::page.template_id') }}"><i class="fa fa-question-circle"></i></span>
          {{ Form::label('template_id', 'Template') }}
          {{ Form::select('template_id', $template_options, null, ['required']) }}
          @if($errors->has('template_id'))
          <small class="error">{{ $errors->first('template_id') }}</small>
          @else
          <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Template']) }}</small>
          @endif

          @if(Config::get('pages::sections.short_description'))
          <div class="short-description-field @if($errors->has('short_description'))error @endif">
            {{ Form::label('short_description', 'Short Description') }}
            {{ Form::text('short_description', null, ['class' => 'radius']) }}
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Short Description']) }}</small>
          </div>
          @endif

          @if(Config::get('pages::sections.url'))
          <div class="url-field @if($errors->has('url'))error @endif">
            {{ Form::label('url', 'URL') }}
            {{ Form::text('url', null, ['class' => 'radius']) }}
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'URL']) }}</small>
          </div>
          @endif

          @if(Config::get('pages::sections.description'))
          <div class="description-field @if($errors->has('description'))error @endif">
            {{ Form::label('description', 'Content') }}
            {{ Form::textarea('description', $section->description, array('class' => 'froala-textarea')) }}
          </div>
          @endif

          @if(Config::get('pages::sections.hover_description'))
          <div class="hover-description-field @if($errors->has('hover_description'))error @endif">
            {{ Form::label('hover_description', 'Hover Content') }}
            {{ Form::textarea('hover_description', $section->hover_description, array('class' => 'froala-textarea')) }}
          </div>
          @endif

          {{ Form::submit('Save', ['class' => 'button tiny success']) }}
          <a class="button tiny alert delete" href="/page/{{ $page->full_permalink }}/section/{{ $section->id }}"><i class="fa fa-trash"></i> Delete</a>

          <div id="image-add-modal-{{ $section->id }}" class="reveal-modal" data-reveal aria-labelledby="File Manager" aria-hidden="true" role="dialog">
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <iframe src="/responsive_file_manager/filemanager/dialog.php?type=1&field_id=image-add-{{ $section->id }}"></iframe>
          </div>
        </div>
      </div>
    </div>
  {{ Form::close() }}
</div>
