<?php
//PageSectionController routes
Route::get('page/{full_permalink}/sections', 'WorkInProgress\Pages\PageSectionController@index')->where('full_permalink', '(.*)');
Route::post('page/{full_permalink}/section', 'WorkInProgress\Pages\PageSectionController@store')->where('full_permalink', '(.*)');
Route::put('page/{full_permalink}/section/{id}', 'WorkInProgress\Pages\PageSectionController@update')->where('full_permalink', '(.*)');
Route::delete('page/{full_permalink}/section/{id}', 'WorkInProgress\Pages\PageSectionController@delete')->where('full_permalink', '(.*)');

//PageImageController routes
Route::get('page/{full_permalink}/images', 'WorkInProgress\Pages\PageImageController@index')->where('full_permalink', '(.*)');
Route::post('page/{full_permalink}/image', 'WorkInProgress\Pages\PageImageController@store')->where('full_permalink', '(.*)');
Route::put('page/{full_permalink}/image/{id}', 'WorkInProgress\Pages\PageImageController@update')->where('full_permalink', '(.*)');
Route::delete('page/{full_permalink}/image/{id}', 'WorkInProgress\Pages\PageImageController@delete')->where('full_permalink', '(.*)');

//PageController routes
Route::get('pages', 'WorkInProgress\Pages\PageController@index');
Route::get('page/{full_permalink}/delete', 'WorkInProgress\Pages\PageController@delete')->where('full_permalink', '(.*)');
Route::get('page/{full_permalink}/edit', 'WorkInProgress\Pages\PageController@edit')->where('full_permalink', '(.*)');
Route::put('page/{full_permalink}/order', 'WorkInProgress\Pages\PageController@order')->where('full_permalink', '(.*)');
Route::put('page/{full_permalink}', 'WorkInProgress\Pages\PageController@update')->where('full_permalink', '(.*)');
Route::resource('page', 'WorkInProgress\Pages\PageController');

?>
